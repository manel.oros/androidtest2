package com.example.loginExample.model

enum class OperationType {
    NEW,
    EDIT,
    DELETE
}