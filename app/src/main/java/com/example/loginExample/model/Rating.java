/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.loginExample.model;

import java.io.Serializable;
import java.time.LocalDate;

/**
 *
 * @author manel
 */

public class Rating implements Serializable{

    private long id;
    
    private String comments;

    private Integer points;

    private Product product;

    public Rating() {
    }

    public Rating(String comments, Integer points) {
        this.comments = comments;
        this.points = points;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
    
}
