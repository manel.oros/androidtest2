package com.example.loginExample.data

data class MyMessage(val title: String, val body: String)