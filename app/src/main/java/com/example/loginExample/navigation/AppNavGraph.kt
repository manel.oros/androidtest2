package com.example.loginExample.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.navigation
import com.example.apirestnosecure.apirest.ProductApi
import com.example.loginExample.screens.Entity1ListScreen
import com.example.loginExample.screens.Entity2ListScreen
import com.example.loginExample.screens.Entity3ListScreen
import com.example.loginExample.screens.HomeScreen
import com.example.loginExample.screens.LoginScreen
import com.example.loginExample.screens.ShowMessage
import com.example.loginExample.screens.ProductsDeleteScreen
import com.example.loginExample.screens.ProductsEditScreen
import com.example.loginExample.screens.ProductsListScreen
import com.example.loginExample.screens.ProductsNewScreen
import com.example.loginExample.screens.RegisterScreen
import com.example.loginExample.screens.SharedViewModel


/***
 * Funciones de NavGraphBuilder:
 * - Construir la Jerarquía de Destinos de Navegación
 * - Definir las Rutas de Navegación
 * - Establecer el Destino Inicial:
 * - Configurar Acciones de Navegación
 * - Navegación Condicional y Argumentos
 * - Subgráficos de Navegación
 * - Gestión del Estado de la Navegación
 *
 * Este gráfico está formado por:
 *
 *  *Home
 *  * │
 *  * ├── Login
 *  * ├── Register
    * ├── Productos
 *  * │   ├── New
 *  * │   ├── Edit
 *  * │   └── Delete
 *  * │
 *  * ├── Entidad1
 *  * │   ├── New
 *  * │   ├── Edit
 *  * │   └── Delete
 *  * │
 *  * ├── Entidad2
 *  * │   ├── New
 *  * │   ├── Edit
 *  * │   └── Delete
 *  * │
 *  * └── Entidad3
 *  *     ├── New
 *  *     ├── Edit
 *  *     └── Delete
 *
 */

/***
 * Elemento composable que va cambiando en la parte central
 * en función de la nagevación solicitada mediante navController
 */
@Composable
fun AppNavGraph(
    navController: NavHostController,
    sharedViewModel: SharedViewModel,
    productApi: ProductApi
) {
    /***
     * Administra y renderiza dinámicamente los composables correspondientes a cada destino en tu aplicación
     */
    NavHost(
        navController = navController,
        startDestination = Screens.Home.route
    )

    /***
     * Bloques NavGraphBuilder con cada destino y su ruta asociada,
     * que puede usarse para navegar entre pantallas
     */
    {
        composable(route = Screens.Home.route) {
            HomeScreen()
        }

        addLoginRoute(navController)
        addProductsRoute(navController, sharedViewModel, productApi )
        addEntity1Route(navController)
        addEntity2Route(navController)
        addEntity3Route(navController)
    }
}

/***
 * Subgráfico Login.
 * el nombre del subgráfico ha de ser único (route = Screens.LoginRoot.route)
 * y no puede conoindicir con el punto de inicio del mismo: startDestination = Screens.Login.route
 * Luego se entran las rutas a las diferentes pantallas como composables: composable(route = Screens.Login.route)
 * Los parámetros que se pasan a las pantallas son delegados a funciones que se ejecutarán
 * dentro de la misma, para enrutar a otras pantallas.
 */
private fun NavGraphBuilder.addLoginRoute(navController: NavController) {
    navigation(
        route = Screens.LoginRoot.route,
        startDestination = Screens.Login.route,
    ) {
        composable(route = Screens.Login.route) {
            LoginScreen(
                goRegister = {
                    navController.navigate(Screens.Register.route)
                },
                loginOk = {
                    navController.navigate(Screens.Home.route)
                }
            )
        }

        composable(route = Screens.Register.route) {
            RegisterScreen(
                onBack = {
                    navController.navigateUp()
                }
            )
        }
    }
}

private fun NavGraphBuilder.addProductsRoute(navController: NavController,
                                            sharedViewModel: SharedViewModel,
                                             productApi: ProductApi) {
    navigation(
        route = Screens.ProductsRoot.route,
        startDestination = Screens.ProductsList.route,
    ) {
        composable(route = Screens.ProductsList.route) {

            ProductsListScreen(
                onError = { message -> navController.navigate("${Screens.ShowMessage.route}/$message") },
                onBack = { navController.navigateUp() },
                sharedViewModel = sharedViewModel,
                productApi = productApi
            )
        }

        composable(route = Screens.ProductsEdit.route)
        {
            ProductsEditScreen(
                //esta es la forma de recoger el parámetro pasado en la llamada a onError en en ProductsDeleteScreen y enviarlo mediante la llamada de navigate
                //un poco rollo, pero al fín y al cabo, bastante conciso
                onError = { message -> navController.navigate("${Screens.ShowMessage.route}/$message")},
                onBack = {navController.navigateUp()},
                sharedViewModel = sharedViewModel,
                productApi = productApi,
                buttonText = "Modificar"
            )
        }

        composable(route = Screens.ProductsDelete.route) {

            ProductsDeleteScreen(
                //esta es la forma de recoger el parámetro pasado en la llamada a onError en en ProductsDeleteScreen y enviarlo mediante la llamada de navigate
                //un poco rollo, pero al fín y al cabo, bastante conciso
                onError = { message -> navController.navigate("${Screens.ShowMessage.route}/$message")},
                onBack = {navController.navigateUp()},
                sharedViewModel = sharedViewModel,
                productApi = productApi,
                buttonText = "Eliminar"
            )
        }

        composable(route = Screens.ProductsNew.route) {

            ProductsNewScreen(
                //esta es la forma de recoger el parámetro pasado en la llamada a onError en en ProductsDeleteScreen y enviarlo mediante la llamada de navigate
                //un poco rollo, pero al fín y al cabo, bastante conciso
                onError = { message -> navController.navigate("${Screens.ShowMessage.route}/$message")},
                onBack = {navController.navigateUp()},
                sharedViewModel = sharedViewModel,
                productApi = productApi,
                buttonText = "Crear"
            )
        }

        composable(route = "${Screens.ShowMessage.route}/{message}") {

            backStackEntry ->
            val message = backStackEntry.arguments?.getString("message") ?: ""

            ShowMessage({navController.navigateUp()}, message)
        }
    }
}

private fun NavGraphBuilder.addEntity1Route(navController: NavController) {
    navigation(
        route = Screens.Entity1Root.route,
        startDestination = Screens.Entity1List.route,
    ) {
        composable(route = Screens.Entity1List.route) {

            Entity1ListScreen(
                onBack = {
                    navController.navigateUp()
                }
            )
        }
    }
}

private fun NavGraphBuilder.addEntity2Route(navController: NavController) {
    navigation(
        route = Screens.Entity2Root.route,
        startDestination = Screens.Entity2List.route,
    ) {
        composable(route = Screens.Entity2List.route) {

            Entity2ListScreen(
                onBack = {
                    navController.navigateUp()
                }
            )
        }
    }
}

private fun NavGraphBuilder.addEntity3Route(navController: NavController) {
    navigation(
        route = Screens.Entity3Root.route,
        startDestination = Screens.Entity3List.route,
    ) {
        composable(route = Screens.Entity3List.route) {

            Entity3ListScreen(
                onBack = {
                    navController.navigateUp()
                }
            )
        }
    }
}












