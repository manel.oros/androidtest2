package com.example.loginExample.navigation

sealed class Screens (val route: String){
    data object HomeRoot : Screens("home_root")
    data object Home : Screens("home")
    data object ShowMessage : Screens("showMessage")

    data object LoginRoot : Screens("login_root")
        data object Login : Screens("login")
        data object Register : Screens("register")

    data object ProductsRoot : Screens("products_root")
        data object ProductsList : Screens("products_list")
        data object ProductsNew : Screens("products_new")
        data object ProductsEdit : Screens("products_edit")
        data object ProductsDelete : Screens("products_delete")

    data object Entity1Root: Screens("entity1_root")
        data object Entity1List : Screens("entity1_list")
        data object Entity1New : Screens("entity1_new")
        data object Entity1Edit : Screens("entity1_Edit")
        data object Entity1Delete : Screens("entity1_Delete")

    data object Entity2Root: Screens("entity2_root")
        data object Entity2List : Screens("entity2_list")
        data object Entity2New : Screens("entity2_new")
        data object Entity2Edit : Screens("entity2_Edit")
        data object Entity2Delete : Screens("entity2_Delete")

    data object Entity3Root: Screens("entity3_root")
        data object Entity3List : Screens("entity3_list")
        data object Entity3New : Screens("entity3_new")
        data object Entity3Edit : Screens("entity3_Edit")
        data object Entity3Delete : Screens("entity3_Delete")

    //pantallas independientes
}