package com.example.apirestnosecure.apirest


import com.example.loginExample.model.Product
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path

interface ProductApi {

    @GET("all")
    suspend fun getAllProducts(): Response<List<Product>>

    @GET("get/{prodId}")
    suspend fun getProductById(@Path("prodId") prodId: Long): Response<Product>

    @POST("create")
    suspend fun createProduct(@Body product: Product): Response<Long>

    @PUT("update")
    suspend fun updateProduct(@Body product: Product): Response<Void>

    @DELETE("delete/{prodId}")
    suspend fun deleteProduct(@Path("prodId") prodId: Long): Response<Void>
}