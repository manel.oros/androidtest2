package com.example.myapplication.apirest

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/***
 * Un objecte a kotlin és una sola instancia d'una classe.
 * És similar a una classe singleton de Java.
 * Si no està contingut a cap classe, la seva visibilitat per defecta és a nivell de package
 */
object RetrofitInstance {

    //Cuando se usa el emulador de movil desde AndroidStudio, la 10.0.2.2 de la red virtual apunta a localhost de la máquina anfitrion
    private const val BASE_URL = "http://10.0.2.2:8080/rest/products/"  // Reemplaza con la URL de tu servidor Spring Boot

    //private const val BASE_URL = "http://192.168.1.144:8080/rest/products/"  // Reemplaza con la URL de tu servidor Spring Boot

    val retrofitInstance: Retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())  // Convierte JSON a objetos Kotlin
            .build()
    }
}