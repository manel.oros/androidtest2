package com.example.loginExample

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import com.example.loginExample.screens.MainScreen
import com.example.loginExample.ui.theme.LoginExampleTheme

//https://medium.com/@waseemabbas8/implement-nested-navigation-with-bottom-navigation-bar-in-android-jetpack-compose-7cd0efbe08ad
//https://developer.android.com/guide/navigation/design/nested-graphs
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            LoginExampleTheme {
               MainScreen()
            }
        }
    }
}

