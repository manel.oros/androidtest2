package com.example.loginExample.screens

import android.annotation.SuppressLint
import android.util.Log
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import com.example.apirestnosecure.apirest.ProductApi
import com.example.loginExample.model.Product
import kotlinx.coroutines.launch

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun ProductsNewScreen(
    onError: (String) -> Unit, //qué hacer si hay un error
    onBack: () -> Unit,
    sharedViewModel: SharedViewModel,
    productApi: ProductApi,
    buttonText: String

) {
    val coroutineScope = rememberCoroutineScope()

    Scaffold {
        Box(modifier = Modifier.fillMaxSize()) {

            // Creamos un nuevo producto vacío
            var newProduct = Product()
            newProduct.price = 0.0
            newProduct.quantity = 0
            newProduct.title = ""
            newProduct.id = 0

            Formulario(
                product = newProduct,
                onAccept = { createdProduct ->
                    coroutineScope.launch {
                        try {
                            val response = productApi.createProduct(createdProduct) //enviamos
                            if (!response.isSuccessful) {
                                onError("ERROR:" + response.code().toString())
                            }
                            else
                                onBack() //... y dew!!
                        } catch (e: Exception) {
                            onError("ERROR:" + e.message)
                        }
                    }
                },
                readOnly = false,
                buttonText = buttonText
            )
        }
    }
}