package com.example.loginExample.screens

import android.annotation.SuppressLint
import android.util.Log
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.example.apirestnosecure.apirest.ProductApi
import com.example.loginExample.model.Product
import com.example.myapplication.apirest.RetrofitInstance
import kotlinx.coroutines.launch

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun ProductsListScreen(
    onError: (String) -> Unit, //qué hacer si hay un error
    onBack: () -> Unit, //delegado que va a ejecutarse en el lado navGraph
    sharedViewModel: SharedViewModel, //para compartir datos entre pantallas
    productApi: ProductApi //conexión con los datos remotos

    ) {

        val products = remember { mutableStateListOf<Product>() } // Lista para almacenar productos
        val coroutineScope = rememberCoroutineScope()
        val serverError = false

        //permite que al modificarse la propiedad de sharedViewModel observada (selectedProduct)
        //se recompongan los componentes visuales afectados (composables)
        val collectSelectedProduct by sharedViewModel.selectedProduct.collectAsState()

        LaunchedEffect(Unit) {
            coroutineScope.launch {
                try {
                    val response = productApi.getAllProducts()
                    if (response.isSuccessful) {
                        val fetchedProducts = response.body()?: emptyList() // Llamada al endpoint
                        products.clear()
                        products.addAll(fetchedProducts.toList())
                    }
                    else
                    {
                        onError("ERROR:" + response.code().toString())
                    }
                } catch (e: Exception) {
                    onError("ERROR:" + e.message)
                }
            }
        }

        Scaffold {
            Box(modifier = Modifier.fillMaxSize()) {

                if (products.isEmpty()) {
                    Text(
                        modifier = Modifier.align(Alignment.Center),
                        text = "No data",
                        style = MaterialTheme.typography.headlineMedium
                    )
                } else {
                    //si todavía no hay un producto seleccionado, queda seleccionado el primero de la lista
                    if (collectSelectedProduct == null)
                    {
                        sharedViewModel.selectProduct(products[0])
                    }
                    LazyColumn(modifier = Modifier.fillMaxSize()) {
                        items(products) { product ->
                            ProductItem(
                                product = product,
                                isSelected = collectSelectedProduct == product,
                                onClick = {
                                    sharedViewModel.selectProduct(product)
                                }
                            )
                        }
                    }
                }
            }
        }
    }

    // Composable para mostrar un producto individual
    @Composable
    fun ProductItem(
        product: Product,
        isSelected: Boolean,
        onClick: () -> Unit
    ) {

        Card(
            modifier = Modifier
                .fillMaxWidth()
                .padding(8.dp)
                .clickable { onClick() },
            elevation = CardDefaults.cardElevation(4.dp),
            colors = CardDefaults.cardColors(
                containerColor = if (isSelected) Color.LightGray else MaterialTheme.colorScheme.surface // Cambiar color de fondo si está seleccionado
            )
        ) {
            Column(modifier = Modifier.padding(16.dp)) {
                Text(text = product.title, style = MaterialTheme.typography.titleMedium)
                Text(text = "$${product.price}", style = MaterialTheme.typography.bodyMedium)
                Text(text = "Cantidad: ${product.quantity}", style = MaterialTheme.typography.bodyMedium)
            }
        }
    }