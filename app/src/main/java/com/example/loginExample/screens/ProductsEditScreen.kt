package com.example.loginExample.screens

import android.annotation.SuppressLint
import android.util.Log

import androidx.compose.foundation.layout.Box

import androidx.compose.foundation.layout.fillMaxSize

import androidx.compose.material3.Scaffold

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState

import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import com.example.apirestnosecure.apirest.ProductApi
import kotlinx.coroutines.launch

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun ProductsEditScreen(
    onError: (String) -> Unit, //qué hacer si hay un error
    onBack: () -> Unit,
    sharedViewModel: SharedViewModel,
    productApi: ProductApi,
    buttonText: String

) {
    val coroutineScope = rememberCoroutineScope()

    Scaffold {
        Box(modifier = Modifier.fillMaxSize()) {

            // Obtenemos el producto seleccionado del ViewModel
            val selectedProduct = sharedViewModel.selectedProduct.collectAsState().value

            if (selectedProduct != null) {
                Formulario(
                    product = selectedProduct,
                    onAccept = { updatedProduct ->
                        coroutineScope.launch {
                            try {
                                val response = productApi.updateProduct(updatedProduct) //actualizamos
                                if (!response.isSuccessful) {
                                    onError("ERROR:" + response.code().toString())
                                }
                                else
                                    onBack() //... y dew!!
                            } catch (e: Exception) {
                                onError("ERROR:" + e.message)
                            }
                        }
                    },
                    readOnly = false,
                    buttonText = buttonText
                )
            }
        }
    }
}