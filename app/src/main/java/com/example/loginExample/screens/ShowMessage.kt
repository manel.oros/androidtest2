package com.example.loginExample.screens

import android.annotation.SuppressLint
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun ShowMessage(onBack: () -> Unit, message: String) {
    Scaffold {
        Box(modifier = Modifier.fillMaxSize()) {

            Text(
                modifier = Modifier.align(Alignment.Center),
                text = message, style = MaterialTheme.typography.titleLarge
            )

            Button(
                modifier = Modifier
                    .align(Alignment.BottomCenter)
                    .padding(16.dp),
                onClick = {onBack()}
            ) {
                Text("Cerrar")
            }
        }
    }
}