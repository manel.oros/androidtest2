package com.example.loginExample.screens

import android.util.Log
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material.icons.filled.Face
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.List
import androidx.compose.material.icons.filled.Lock
import androidx.compose.material3.BottomAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.MutableIntState
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.Stable
import androidx.compose.runtime.State
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.example.apirestnosecure.apirest.ProductApi
import com.example.loginExample.model.OperationType
import com.example.loginExample.navigation.AppNavGraph
import com.example.loginExample.navigation.Screens
import com.example.myapplication.apirest.RetrofitInstance


/***
 * Pantalla marco que contiene las intrapantallas.
 * - Tiene una barra superior y una inferior

 *
 * Se habilita un sistema de eventos que permite recargar la pantalla principal
 * cada vez que se cambia de subpantalla.
 */
@Composable
fun MainScreen() {
    /***
     * rememberNavController
     * - Actua com singleton para recordar la misma instancia durante las recomposiciones,
     *   lo que garantiza que el estado de navegación no se pierda.
     */
    val navController = rememberNavController()

    val screenLevel by navController.currentScreenAsState().first
    val currentSelectedScreen by navController.currentScreenAsState().second
    val sharedViewModel: SharedViewModel = viewModel()

    //creación de la conexión con el servicio remoto
    val retrofit = RetrofitInstance.retrofitInstance
    val productApi = retrofit.create(ProductApi::class.java)

    Scaffold(
        topBar = {
            /***
             * La lògica és molt simple i per tant no passem paràmetre sinó que
             * ocultem directament
             */
            if (screenLevel != 0) {
                TopNavBar(
                    navController = navController,
                    currentSelectedScreen = currentSelectedScreen
                )
            }
        },

        bottomBar = {
            BottomNavBar(
                navController = navController,
                currentSelectedScreen = currentSelectedScreen,
                level = screenLevel
            )
        },
        modifier = Modifier.fillMaxSize(),

        content = { paddingValues ->
            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(paddingValues)
            ) {
                AppNavGraph(navController, sharedViewModel, productApi) //<--- aquí se aplica el contenido de cada pantalla
            }
        }
    )
}

/***
 * * - La barra inferior cambia en función del modo de pantalla en la que estamos:
 *  *    Level == 0: Todos los iconos ocultos
 *  *    Level == 1: Home + menú
 *  *    Level == 2: Home + CRUD
 */
@Composable
private fun BottomNavBar(
    navController: NavController,
    currentSelectedScreen: Screens,
    level : Int
) {
    NavigationBar {
        Log.i("JetpackCompose", "Level: ${level}")
        BottomAppBar(
            actions = {

                if (level != 0) {
                    IconButton(onClick = {
                        navController.navigate(Screens.Home.route)
                    }) {
                        Icon(
                            Icons.Filled.Home,
                            contentDescription = "Menú principal"
                        )
                    }
                }

                //nivell menú principal
                if (level == 1) {
                    IconButton(onClick = {
                        navController.navigate(Screens.ProductsList.route)
                    }
                    ) {
                        Icon(
                            Icons.Filled.List,
                            contentDescription = "Listar productos",
                        )
                    }

                    IconButton(onClick = {
                        navController.navigate(Screens.Entity1List.route)
                    }
                    ) {
                        Icon(
                            Icons.Filled.Face,
                            contentDescription = "Listar entidad1",
                        )
                    }

                    IconButton(onClick = {
                        navController.navigate(Screens.Entity2List.route)
                    }
                    ) {
                        Icon(
                            Icons.Filled.Face,
                            contentDescription = "Listar entidad2",
                        )
                    }

                    IconButton(onClick = {
                        navController.navigate(Screens.Entity3List.route)
                    }
                    ) {
                        Icon(
                            Icons.Filled.Face,
                            contentDescription = "Listar entidad3",
                        )
                    }
                }

                //nivell CRUD
                if (level == 2) {
                    IconButton(onClick = {
                        navController.navigateToLevel2Screen(currentSelectedScreen, OperationType.NEW)
                    }
                    ) {
                        Icon(
                            Icons.Filled.Add,
                            contentDescription = "Localized description",
                        )
                    }

                    IconButton(onClick = {
                        navController.navigateToLevel2Screen(currentSelectedScreen, OperationType.EDIT)
                    }
                    ) {
                        Icon(
                            Icons.Filled.Edit,
                            contentDescription = "Localized description",
                        )
                    }

                    IconButton(onClick = {
                        navController.navigateToLevel2Screen(currentSelectedScreen, OperationType.DELETE)
                    }
                    ) {
                        Icon(
                            Icons.Filled.Delete,
                            contentDescription = "Localized description",
                        )
                    }
                }
            },
        )
    }
}

/***
 * La barra superior cambia en función del modo de pantalla en la que estamos:
 *  Level == 0: Todos los iconos ocultos
 *  Level == 1: Back + exit
 *  Level == 2: Back + exit
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
private fun TopNavBar(
    navController: NavController,
    currentSelectedScreen: Screens
) {
    NavigationBar {
        TopAppBar(

            actions = {
                IconButton(onClick = {
                    navController.navigate(Screens.LoginRoot.route) }
                ) {
                    Icon(
                        Icons.Filled.Lock,
                        contentDescription = "Localized description",
                    )
                }
            },
            colors = TopAppBarDefaults.topAppBarColors(
                containerColor = MaterialTheme.colorScheme.primaryContainer,
                titleContentColor = MaterialTheme.colorScheme.primary,
            ),
            title = {
                Text("")
            },

            navigationIcon = {
                IconButton(onClick = { navController.popBackStack() }) {
                    Icon(
                        imageVector = Icons.Filled.ArrowBack,
                        contentDescription = "Localized description"
                    )
                }
            }
        )
    }
}

@Stable
@Composable
private fun NavController.currentScreenAsState(): Pair<MutableIntState, MutableState<Screens>> {
    var level = remember { mutableIntStateOf(0) };
    val selectedItem = remember { mutableStateOf<Screens>(Screens.HomeRoot) }
    DisposableEffect(key1 = this) {
        val listener = NavController.OnDestinationChangedListener { _, destination, _ ->

            Log.i("JetpackCompose", "Cambiando de pantalla a " + destination.route)

            val route = destination.route

            if (route != null) {
                when {
                    route.contains(Screens.Login.route) -> {
                        selectedItem.value = Screens.Login
                        level.intValue = 0
                    }

                    route.contains(Screens.ShowMessage.route) -> {
                        selectedItem.value = Screens.ShowMessage
                        level.intValue = 0
                    }

                    route.contains(Screens.Home.route) -> {
                        selectedItem.value = Screens.Home
                        level.intValue = 1
                    }

                    route.contains(Screens.ProductsList.route) -> {
                        selectedItem.value = Screens.ProductsList
                        level.intValue = 2
                    }

                    route.contains(Screens.ProductsEdit.route) -> {
                        selectedItem.value = Screens.ProductsEdit
                        level.intValue = 2
                    }

                    route.contains(Screens.ProductsNew.route) -> {
                        selectedItem.value = Screens.ProductsNew
                        level.intValue = 2
                    }

                    route.contains(Screens.ProductsDelete.route) -> {
                        selectedItem.value = Screens.ProductsDelete
                        level.intValue = 2
                    }

                    route.contains(Screens.Entity1List.route) -> {
                        selectedItem.value = Screens.Entity1List
                        level.intValue = 2
                    }

                    route.contains(Screens.Entity2List.route) -> {
                        selectedItem.value = Screens.Entity2List
                        level.intValue = 2
                    }

                    route.contains(Screens.Entity3List.route) -> {
                        selectedItem.value = Screens.Entity3List
                        level.intValue = 2
                    }
                }
            }
        }
        addOnDestinationChangedListener(listener)
        onDispose {
            removeOnDestinationChangedListener(listener)
        }
    }
    return Pair(first = level, second = selectedItem)
}

@Stable
@Composable
private fun NavController.currentRouteAsState(): State<String?> {
    val selectedItem = remember { mutableStateOf<String?>(null) }
    DisposableEffect(this) {
        val listener = NavController.OnDestinationChangedListener { _, destination, _ ->
            selectedItem.value = destination.route
        }
        addOnDestinationChangedListener(listener)

        onDispose {
            removeOnDestinationChangedListener(listener)
        }
    }
    return selectedItem
}

/***
 * Dirige la navegación a las pantallas de tipo CRUD
 * en función del menú
 *
 * TODO: Cambiar por la navegación integrada en AppNavGraph
 */
private fun NavController.navigateToLevel2Screen(screen: Screens, op: OperationType)
{
    when (screen) {

        Screens.ProductsList -> {
            when(op)
            {
                OperationType.NEW -> navigate(Screens.ProductsNew.route)
                OperationType.EDIT -> navigate(Screens.ProductsEdit.route)
                OperationType.DELETE -> navigate(Screens.ProductsDelete.route)

                else -> {
                    Log.e("JetpackCompose", "Pendiente de implementar $op sobre $screen")
                }
            }
        }

        Screens.Entity1List -> {
            when(op)
            {
                //aqui el CRUD d'entitat 1

                else -> {
                    Log.e("JetpackCompose", "Pendiente de implementar $op sobre $screen")
                }
            }
        }

        Screens.Entity2List -> {
            when(op)
            {
                //aqui el CRUD d'entitat 2

                else -> {
                    Log.e("JetpackCompose", "Pendiente de implementar $op sobre $screen")
                }
            }
        }

        Screens.Entity3List -> {
            when(op)
            {
                //aqui el CRUD d'entitat 3

                else -> {
                    Log.e("JetpackCompose", "Pendiente de implementar $op sobre $screen")
                }
            }
        }

        else -> {
            Log.e("JetpackCompose", "Pendiente de implementar $op sobre $screen")
        }
    }
}