package com.example.loginExample.screens

import androidx.lifecycle.ViewModel
import com.example.loginExample.model.Product
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

/***
 *  Clase que permite compartir datos entre diferentes pantallas
 *  y al mismo tiempo, éstos son sensibles a cualquier cambio y desencadenan
 *  la recomposición de los componentes.
 *
 *  ViewModel: arquitectura de Jetpack para almacenar y gestionar datos relacionados con la UI de manera eficiente,
 *  especialmente cuando los datos necesitan sobrevivir a cambios de configuración como la rotación de la pantalla.
 */
class SharedViewModel : ViewModel() {

    /***
     * MutableStateFlow es una variante de StateFlow que permite modificar el valor del flujo,
     * es decir, puedes actualizar su valor en cualquier momento
     */
    private val _selectedProduct = MutableStateFlow<Product?>(null)

    /***
     * Lo convierte de nuevo en no mutable.
     * Sólo puede leerse, pero no modificarse fuera del ámbito de la clase.
     */
    val selectedProduct = _selectedProduct.asStateFlow()

    //setter
    fun selectProduct(product: Product) {
        _selectedProduct.value = product
    }

    fun clearSelectedProduct() {
        _selectedProduct.value = null
    }
}