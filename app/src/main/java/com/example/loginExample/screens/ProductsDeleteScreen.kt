package com.example.loginExample.screens

import android.annotation.SuppressLint
import android.util.Log
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.Button
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.example.apirestnosecure.apirest.ProductApi
import kotlinx.coroutines.launch


@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun ProductsDeleteScreen(
    onError: (String) -> Unit, //qué hacer si hay un error
    onBack: () -> Unit, //qué hacer si todo ok
    sharedViewModel: SharedViewModel,
    productApi: ProductApi,
    buttonText: String

) {
    val coroutineScope = rememberCoroutineScope()

    Scaffold {
        Box(modifier = Modifier.fillMaxSize()) {

            // Obtenemos el producto seleccionado del ViewModel
            val selectedProduct = sharedViewModel.selectedProduct.collectAsState().value

            if (selectedProduct != null) {
                Formulario(
                    product = selectedProduct,
                    onAccept = { deletedProduct ->
                        coroutineScope.launch {
                            try {
                                val response = productApi.deleteProduct(deletedProduct.id) //actualizamos
                                if (!response.isSuccessful) {
                                    onError("ERROR:" + response.code().toString())
                                }
                                else {
                                    sharedViewModel.clearSelectedProduct()
                                    onBack() //... y dew!!
                                }
                            } catch (e: Exception) {
                                onError("ERROR:" + e.message)
                            }
                        }
                    },
                    readOnly = true,
                    buttonText = buttonText
                )
            }
        }
    }
}