package com.example.loginExample.screens

import android.util.Log
import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.delay


/***
 * The purpose of the ViewModel is to acquire and keep the information that is necessary for an Activity or a Fragment. The Activity or the Fragment should be able to observe changes in the ViewModel. ViewModels usually expose this information via androidx.lifecycle.LiveData or Android Data Binding
 * ViewModel's only responsibility is to manage the data for the UI. It should never access your view hierarchy or hold a reference back to the Activity or the Fragment.
 */
//https://youtu.be/EmUx8wgRxJw?t=1827
//https://developer.android.com/codelabs/basic-android-kotlin-training-livedata?hl=es-419#0
class LoginViewModel : ViewModel(){

    private val _email = MutableLiveData<String>()
    val email : LiveData<String> = _email

    private val _password = MutableLiveData<String>()
    val password : LiveData<String> = _password

    //valor que habilita d deshabilita el boton de login
    private val _loginEnable = MutableLiveData<Boolean>()
    val loginEnable : LiveData<Boolean> = _loginEnable

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading : LiveData<Boolean> = _isLoading

    private val _isLoginOk = MutableLiveData<Boolean>()
    val isLoginOk : LiveData<Boolean> = _isLoginOk

    fun onLoginChanged(email: String, password: String) {
        //TODO: cambiar en modo produccion
        _email.value = email
        _password.value = password
        _loginEnable.value = isValidFormatEmail(email) && isValidFormatPassword(password)

    }

    private fun isValidFormatPassword(password: String): Boolean = password.length >= 6

    private fun isValidFormatEmail(email: String): Boolean = Patterns.EMAIL_ADDRESS.matcher(email).matches()

    suspend fun onLoginSelected() {

        _isLoading.value = true
        Log.i("login values=",email.value.toString()+" "+password.value.toString() )

       /* if ((email.value == "manel.oros@copernic.cat") && (password.value == "12345678"))*/
        if (true)
        {
            _isLoginOk.value = true
        }else
        {
            delay(2000)
        }
        _isLoading.value = false
    }
}