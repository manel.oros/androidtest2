package com.example.loginExample.screens

import android.annotation.SuppressLint
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.darkColorScheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.loginExample.R
import com.example.loginExample.common.observeAsState
import com.example.loginExample.ui.theme.Pink40
import com.example.loginExample.ui.theme.Pink80
import com.example.loginExample.ui.theme.Purple40
import com.example.loginExample.ui.theme.Purple80
import com.example.loginExample.ui.theme.PurpleGrey40
import com.example.loginExample.ui.theme.PurpleGrey80
import kotlinx.coroutines.launch

private val DarkColorScheme = darkColorScheme( // …1
    primary = Purple80,
    secondary = PurpleGrey80,
    tertiary = Pink80
)

private val LightColorScheme = lightColorScheme( // …1
    primary = Purple40,
    secondary = PurpleGrey40,
    tertiary = Pink40
)

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun LoginScreen(
    goRegister: () -> Unit,
    loginOk: () -> Unit
){

    var viewModel = LoginViewModel()
    Scaffold {
        Box(
            Modifier
                .fillMaxSize()
                .padding(16.dp)
        ) {
            Login(Modifier.align(Alignment.Center), viewModel, loginOk, goRegister)
        }
    }
}

/***
 * logonOk y goRegister son las rutas de navegación a las pantallas correspondientes
 */
@Composable
fun Login(align: Modifier, viewModel: LoginViewModel,  loginOk: () -> Unit, goRegister: () -> Unit) {
    
    val email: String by viewModel.email.observeAsState(initial="")
    val password: String by viewModel.password.observeAsState(initial="")
    val loginEnabled:Boolean by viewModel.loginEnable.observeAsState(initial = true) //TODO: cambiar a false en produccion
    val isLoading:Boolean by viewModel.isLoading.observeAsState(initial = false)
    val isLoginOk:Boolean by viewModel.isLoginOk.observeAsState(initial = false)
    val coroutineScope = rememberCoroutineScope()

    if (isLoading){
        Box(Modifier.fillMaxSize()){
            CircularProgressIndicator(Modifier.align(Alignment.Center))
        }
    }else {

        Column(modifier = align) {
            HeaderImage(Modifier.align(Alignment.CenterHorizontally))
            Spacer(modifier = Modifier.padding(16.dp))
            EmailField(email) { viewModel.onLoginChanged(it, password) }
            Spacer(modifier = Modifier.padding(16.dp))
            PasswordField(password) { viewModel.onLoginChanged(email, it) }
            Spacer(modifier = Modifier.padding(16.dp))
            Register(goRegister)
            Spacer(modifier = Modifier.padding(16.dp))
            LoginButton(loginEnabled) {
                coroutineScope.launch {
                    viewModel.onLoginSelected()
                    if (isLoginOk) {
                        loginOk()
                    }
                }
            }
        }
    }
}

@Composable
fun LoginButton(loginEnable: Boolean, onLoginSelected: () -> Unit) {
    Button(
        enabled = loginEnable,
        onClick = { onLoginSelected() },
        modifier= Modifier
            .fillMaxWidth()
            .height(48.dp)
    ) {
        Text("Login")
    }
}

@Composable
fun Register(goRegister: () -> Unit) {
    Text(text="Register",
        modifier = Modifier.clickable { goRegister() },
        fontSize = 12.sp,
        fontWeight = FontWeight.Bold
    )
}

@Composable
fun PasswordField(password: String, onTextFieldChanged:(String) -> Unit) {
    TextField(
        value = password,
        onValueChange = {onTextFieldChanged(it)},
        modifier=Modifier.fillMaxWidth(),
        placeholder = {Text(text="Password")},
        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password),
        singleLine = true,
        maxLines = 1
    )
}

@Composable
fun EmailField(email: String, onTextFieldChanged:(String) -> Unit) {

    TextField(
         value = email,
         onValueChange = {onTextFieldChanged(it)},
         modifier=Modifier.fillMaxWidth(),
         placeholder = {Text(text="Email")},
         keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Email),
         singleLine = true,
         maxLines = 1
     )
}

@Composable
fun HeaderImage(align: Modifier) {
    Image(painter = painterResource(id = R.drawable.sponge_bob), contentDescription = "Header", modifier = align)
}

