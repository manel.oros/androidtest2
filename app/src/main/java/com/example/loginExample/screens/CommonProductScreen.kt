package com.example.loginExample.screens

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import com.example.loginExample.model.Product

/***
 * Pantalla común para editar o visualizar un producto
 */
@Composable
fun Formulario(product: Product, onAccept: (Product) -> Unit, readOnly: Boolean, buttonText: String) {

    // Estados inicializados con los valores del producto recibido
    var id by remember { mutableStateOf(product.id) }
    var title by remember { mutableStateOf(product.title) }
    var price by remember { mutableStateOf(product.price.toString()) }
    var quantity by remember { mutableStateOf(product.quantity.toString()) }

    // Estados de error para validación
    var priceError by remember { mutableStateOf(false) }
    var quantityError by remember { mutableStateOf(false) }

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp),
        verticalArrangement = Arrangement.spacedBy(8.dp)
    ) {
        // Campo para el id
        OutlinedTextField(
            readOnly = readOnly,
            value = id.toString(),
            onValueChange = { },
            label = { Text("Id") },
            modifier = Modifier.fillMaxWidth()
        )

        // Campo para el título
        OutlinedTextField(
            readOnly = readOnly,
            value = title,
            onValueChange = { title = it },
            label = { Text("Título") },
            modifier = Modifier.fillMaxWidth()
        )

        // Campo para el precio
        OutlinedTextField(
            readOnly = readOnly,
            value = price,
            onValueChange = {
                price = it
                priceError = it.toFloatOrNull() == null // Validación básica
            },
            label = { Text("Precio") },
            modifier = Modifier.fillMaxWidth(),
            isError = priceError,
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number)
        )
        if (priceError) {
            Text(
                text = "Debe ser un número válido.",
                color = MaterialTheme.colorScheme.error,
                style = MaterialTheme.typography.bodySmall
            )
        }

        // Campo para la cantidad
        OutlinedTextField(
            readOnly = readOnly,
            value = quantity,
            onValueChange = {
                quantity = it
                quantityError = it.toIntOrNull() == null // Validación básica
            },
            label = { Text("Cantidad") },
            modifier = Modifier.fillMaxWidth(),
            isError = quantityError,
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number)
        )
        if (quantityError) {
            Text(
                text = "Debe ser un número entero válido.",
                color = MaterialTheme.colorScheme.error,
                style = MaterialTheme.typography.bodySmall
            )
        }

        // Botón de aceptar
        Button(
            onClick = {
                val parsedPrice = price.toDoubleOrNull()
                val parsedQuantity = quantity.toIntOrNull()

                // Validación final antes de aceptar
                if (parsedPrice != null && parsedQuantity != null) {
                    product.id = id
                    product.title = title
                    product.price = parsedPrice.toDouble()
                    product.quantity = parsedQuantity.toInt()
                    onAccept( product )
                }
            },
            modifier = Modifier.fillMaxWidth(),
            enabled = title.isNotBlank() && price.toFloatOrNull() != null && quantity.toIntOrNull() != null
        ) {
            Text(buttonText)
        }
    }
}